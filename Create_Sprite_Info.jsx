﻿/**
* JSON - from: https://github.com/douglascrockford/JSON-js
*/
if(typeof JSON!=='object'){JSON={};}(function(){'use strict';function f(n){return n<10?'0'+n:n;}function this_value(){return this.valueOf();}if(typeof Date.prototype.toJSON!=='function'){Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+'-'+f(this.getUTCMonth()+1)+'-'+f(this.getUTCDate())+'T'+f(this.getUTCHours())+':'+f(this.getUTCMinutes())+':'+f(this.getUTCSeconds())+'Z':null;};Boolean.prototype.toJSON=this_value;Number.prototype.toJSON=this_value;String.prototype.toJSON=this_value;}var cx,escapable,gap,indent,meta,rep;function quote(string){escapable.lastIndex=0;return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];return typeof c==='string'?c:'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4);})+'"':'"'+string+'"';}function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==='object'&&typeof value.toJSON==='function'){value=value.toJSON(key);}if(typeof rep==='function'){value=rep.call(holder,key,value);}switch(typeof value){case'string':return quote(value);case'number':return isFinite(value)?String(value):'null';case'boolean':case'null':return String(value);case'object':if(!value){return'null';}gap+=indent;partial=[];if(Object.prototype.toString.apply(value)==='[object Array]'){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||'null';}v=partial.length===0?'[]':gap?'[\n'+gap+partial.join(',\n'+gap)+'\n'+mind+']':'['+partial.join(',')+']';gap=mind;return v;}if(rep&&typeof rep==='object'){length=rep.length;for(i=0;i<length;i+=1){if(typeof rep[i]==='string'){k=rep[i];v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}else{for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=str(k,value);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}v=partial.length===0?'{}':gap?'{\n'+gap+partial.join(',\n'+gap)+'\n'+mind+'}':'{'+partial.join(',')+'}';gap=mind;return v;}}if(typeof JSON.stringify!=='function'){escapable=/[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'};JSON.stringify=function(value,replacer,space){var i;gap='';indent='';if(typeof space==='number'){for(i=0;i<space;i+=1){indent+=' ';}}else if(typeof space==='string'){indent=space;}rep=replacer;if(replacer&&typeof replacer!=='function'&&(typeof replacer!=='object'||typeof replacer.length!=='number')){throw new Error('JSON.stringify');}return str('',{'':value});};}if(typeof JSON.parse!=='function'){cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;JSON.parse=function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==='object'){for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v;}else{delete value[k];}}}}return reviver.call(holder,key,value);}text=String(text);cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return'\\u'+('0000'+a.charCodeAt(0).toString(16)).slice(-4);});}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,'@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']').replace(/(?:^|:|,)(?:\s*\[)+/g,''))){j=eval('('+text+')');return typeof reviver==='function'?walk({'':j},''):j;}throw new SyntaxError('JSON.parse');};}}());

var baseName = "";
var charName = ""
var lookName = ""
var mirrorable = true
var rightFacing = false

function startsWith(sub, full) {
    if (full.length < sub.length)
        return false;
        
    for (var i = 0; i < sub.length; i++) {
        if (full[i] != sub[i])
            return false;
    }
    return true;
}

function validateLayers() {
    function findLayer(name) {
        var myDoc = app.activeDocument
        for (var i = 0; i < myDoc.layers.length; i++) {
            if (startsWith(name, myDoc.layers[i].name))
                return true;
        }
        alert("Missing layer named \"" + name + "\". Invalid PSD",  "Error!", 1 );
        return false;
    }
    if (!(findLayer("base") && findLayer("face")))
        return false;
    return true;
}

function getUserInput() {
    charName =prompt("What is the name of the character?","enter character name in lowercase","Input Character Name");
    if (null == charName)
        return false;
    lookName = prompt("What is the name of this look?","enter look name in lowercase","Input Look Name");
    if (null == lookName)
        return false;
    mirrorable = confirm("Is this image mirrorable?", false, "Pls Confirm");
    rightFacing = confirm("Is this a right-facing character?", false, "Pls Confirm!");
    return true;
 }

function parseLayers() {
    var result = {};
    var order = [];
    var myDoc = app.activeDocument
    for (var i = 0; i < myDoc.layers.length; i++) {
        var layer = myDoc.layers[i];
        if (layer.name.toLowerCase() == "background") {
            continue;
        }
        var convertedBounds = [];
        for (var j = 0; j < 2; j++) {
            convertedBounds.push(layer.bounds[j].as('px'));
        }
        if (startsWith("base", layer.name))
            baseName = layer.name;
        order.push(layer.name);
        result[layer.name] = convertedBounds;
    }
    order.reverse();

    for (var property in result) {
        if (baseName == property) 
            continue;
        result[property] = [ result[property][0] - result[baseName][0], result[property][1] - result[baseName][1] ];
    }
    result[baseName] = [ 0, 0 ];
    return [order, result];
}

function serializeData() {
    function intArrayToJsonObject(name, array) {
        var out =  "\"" + name + "\": [ ";
        for (var i = 0; i < array.length; i++ ) {
            if (i != 0)
                out += ", ";
            out += "" + array[i]
        }
        return out + " ]"
    }
    var output = {
        "character": charName,
        "name": lookName,
        "mirrorable": mirrorable,
        "right_facing": rightFacing,
        "layers": []
    };

    var result = parseLayers();
    var order = result[0];
    result = result[1];
    
    for (var i = 0; i < order.length; i++) {
        var layer = order[i];
        var blah = "{" + "\"name\": \"" + layer + "\", " + intArrayToJsonObject("offset", result[layer]) + "}";
        output["layers"].push(JSON.parse( blah ));
    }
    
    return JSON.stringify(output);
 }

function exportSprite(json) {
     var myFolder = Folder.selectDialog ("Select a folder");
     if (null == myFolder)
        return false;
        
     var scriptFile = new File(myFolder.fsName + "\\composite_data.json");
     scriptFile.open ("w");
     var res = scriptFile.write(json);
     if (!res)
        $.writeln(scriptFile.error);
     scriptFile.close();
     
     alert("Now run File>Scripts>Export Layers To Files",  "Almost Done!", 0 );
     return true;
}


function main() {
    $.writeln("=== Validate PSD ===")
    if (!validateLayers())
        return;
    $.writeln("=== Get User Input ===")
    if (!getUserInput())
       return;
       
    $.writeln("=== Create JSON Output ===")
    var json = serializeData();
    $.writeln(json);
    
    $.writeln("=== Export Sprite ===")
    exportSprite(json);
}

main();

