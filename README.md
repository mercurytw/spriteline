A toolchain to export composite sprites from photoshop, remove duplicate images, and generate Ren'Py script to compose them

# Python dependencies
Pillow

# Usage
The .jsx file goes in the Presets/Scripts folder in your photoshop install directory. It was written for CS 5

Make sure one layer is called base, and one layer is called face

After you create the images, you need to put the files in appropriate folders

1. create a folder to hold all the faces.
2. For each character, create a folder inside that titled exactly the name of the character
3. Put all corresponding faces for that character in that folder, and name them as you want. For example, the faces/eileen folder might contain surprised.png
4. Make another folder to contain the composite sprites
5. Inside that folder, create separate folders for the different things you've exported. Place the .json file along with any exported layers there
6. If photoshop added a prefix to the layers when it exported them, remove the prefix
7. You can now call the program with python: `python spriteline <path to sprite folder> <path to faces folder> <output folder in your game>`

If you're confused by how the folder structure should look, look in the test directory in this project
