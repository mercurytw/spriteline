"""
Main entry point into program
"""

import hashlib
import json
import os.path
from PIL import Image
import shutil
import sys


class SpriteData(object):
    def __init__(self, in_path):
        # not going to put this in a try. just fail hard
        with open(in_path, "r") as infile:
            self.jsob = json.load(infile)
        for layer in self.jsob['layers']:
            if 'base' in layer['name']:
                layer['is_base'] = True
            else:
                layer['is_base'] = False


class FaceData(object):
    def __init__(self, image_hash, name, size):
        self.hash = image_hash
        self.name = name
        self.size = size


class ParsedResources(object):
    def __init__(self, sprite_path, eye_path):
        self.resource_map = {}
        self.sprites = []
        self.faces = {}
        self._build_sprites_from_path(sprite_path)
        self._build_faces_from_path(eye_path)

    def _build_sprites_from_path(self, path):
        for subdir, _, files in os.walk(path):
            for file in files:
                file_path = subdir + os.sep + file
                if file.endswith(".json"):
                    print("Got file " + file_path)
                    data = SpriteData(file_path)
                    for layer in data.jsob['layers']:
                        name = layer['name']
                        if 'face' == name:
                            continue
                        hasher = hashlib.md5()
                        image_path = subdir + os.sep + name.replace(' ', '-') + '.png'
                        with open(image_path, 'rb') as image:
                            hasher.update(image.read())
                        image_hash = hasher.hexdigest()
                        if image_hash not in self.resource_map:
                            self.resource_map[image_hash] = image_path
                        layer['name'] = image_hash
                        with Image.open(image_path) as im:
                            layer['size'] = im.size
                    self.sprites.append(data)

    def _build_faces_from_path(self, path):
        for subdir, _, files in os.walk(path):
            for file in files:
                image_path = subdir + os.sep + file
                character = os.path.basename(subdir)
                expression = os.path.splitext(file)[0]
                print("Got face " + expression + " for character " + character)
                hasher = hashlib.md5()
                with open(image_path, 'rb') as image:
                    hasher.update(image.read())
                image_hash = hasher.hexdigest()
                if image_hash not in self.resource_map:
                    self.resource_map[image_hash] = image_path
                if character not in self.faces:
                    self.faces[character] = []
                with Image.open(image_path) as im:
                        size = im.size
                self.faces[character].append(FaceData(image_hash, expression, size))


def create_game_folder(folder, resources):
    if not os.path.exists(folder):
        os.mkdir(folder)
    for file in os.listdir(folder):
        path = os.path.join(folder, file)
        try:
            if os.path.isfile(path):
                os.unlink(path)
        except Exception as ex:
            print("Failed to remove file? " + str(ex))
            raise ex
    for key, value in resources.resource_map.items():
        filename = os.path.join(folder, key + '.png')
        shutil.copyfile(value, filename)


def get_offset(x_offt, layer_width, composite_width, mirrored=False):
    if mirrored:
        return composite_width - x_offt - layer_width
    return x_offt


def get_string(js, composite_size, face, mirrored, force_right):
    lhs = "image {0} {1} &&& {2} {3} = ".format(js['character'], js['name'], "right" if mirrored else '',
                                                "right" if force_right else "")
    rhs = "im.Composite( ({0}, {1})".format(*composite_size)
    for layer in js['layers']:
        offset = layer['offset']
        if 'face' == layer['name']:
            rhs += ", ({0}, {1}), $$$".format(get_offset(offset[0], face.size[0], composite_size[0], mirrored),
                                              offset[1])
            continue
        rhs += ", ({0}, {1}), {2}".format(offset[0], offset[1],
                                          "im.Flip(\"" + layer['name'] + '.png\", horizontal=True)' if mirrored else
                                          "\"" + layer['name'] + '.png\"')
    rhs += ")"
    full_line = (lhs + rhs).replace("&&&", face.name)
    full_line = full_line.replace("$$$", "\"" + face.hash + '.png\"')
    return full_line


def construct_sprites(folder, resources):
    for sprite in resources.sprites:
        js = sprite.jsob
        size = [0, 0]  # w, h
        for layer in js['layers']:
            if 'face' == layer['name']:
                continue
            offset = layer['offset']
            im_size = layer['size']
            if im_size[0] + offset[0] > size[0]:
                size[0] = im_size[0] + offset[0]
            if im_size[1] + offset[1] > size[1]:
                size[1] = im_size[1] + offset[1]
        with open(os.path.join(folder, 'dynamic_sprites.rpy'), 'w') as outfile:
            outfile.write("# THIS IS A DYNAMICALLY GENERATED SCRIPT\n")
            outfile.write("# do not modify by hand\n\n")
            for face in resources.faces[js['character']]:
                outfile.write(get_string(js, size, face, False, js['right_facing']) + "\n")
            if js['mirrorable']:
                for face in resources.faces[js['character']]:
                    outfile.write(get_string(js, size, face, True, False) + "\n")


# ./spriteline input-dir output-dir
def main():
    if len(sys.argv) < 3:
        print("usage: {} <sprite-dir> <eye-dir> <output-dir>".format(sys.argv[0]))
        return

    if not os.path.isdir(sys.argv[1]):
        print("{} is not a folder. Please pass a valid input folder")
        return

    if not os.path.isdir(sys.argv[2]):
        print("{} is not a folder. Please pass a valid input folder")
        return

    out_path = sys.argv[3]
    if os.path.isfile(out_path) or not os.path.isdir(os.path.dirname(out_path)):
        print("{} is invalid. Make sure it isnt the name of a file, and the parent path exists")
        return

    resources = ParsedResources(sys.argv[1], sys.argv[2])
    create_game_folder(out_path, resources)
    construct_sprites(out_path, resources)
